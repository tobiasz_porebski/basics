## About
Hello. I'm glad to welcome you in a basic Python3 project's repository.
I will publish here my sample projects created over the years. Some of them were finished quite time ago but never published and some needed a little brush up.

## Project's description
__Fibonacci Sequence__ _[01_fib]_ - very easy sample how to programm the sequence of the great Fibonacci. 

__Cows and Bulls__ _[02_cow]_ - a well known code guessing game. This project have two language options and automaticaly recognize which one to use based on your locale setup.
If somehow you have never heard of Cows and bulls jump in and try it out. Game have a build in section on "how to play".

__Game of Life__ _[03_gol]_ - _WORK STILL IN PROGRESS_ have You ever heard of cellular automaton? In short: __cellular automaton__ consist of a regular grid of cells, each cell has its state and neighbourhood. Each automaton
has specific set of rules that defined when cell can change state. The automatons have many applications in science so if You are interested in further exploration of the topic, please visit [Wikipedia](https://en.wikipedia.org/wiki/Cellular_automaton) for a start point.

__Chaos Game__ _[04_cha] - WORK STILL IN PROGRESS

__Forest Fire__ _[05_for] - WORK STILL IN PROGRESS
	
## Technologies
Just Python3 (with use of default package libraries)
	
## Setup
To run this project, make sure you have Python3 installed on your local machine.
Then download or fork the repository. All samples run in the terminal.